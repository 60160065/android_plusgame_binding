package buu.phatcharapol.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.phatcharapol.calculator.databinding.ActivityPlusBinding
import java.util.*


class PlusActivity : AppCompatActivity() {
    private lateinit var binding: ActivityPlusBinding
    var correct: Int = 0
    var incorrect: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_plus)
        game(correct, incorrect)
    }

    fun game(correctScore: Int, incorrectScore: Int) {
        val trueScore = binding.trueScore
        trueScore.setText("Correct : " + correct.toString())
        val falseScore = binding.falseScore
        falseScore.setText("Incorrect : " + incorrect.toString())

        val num1 = binding.num1
        val num2 = binding.num2
        val btn1 = binding.btn1
        val btn2 = binding.btn2
        val btn3 = binding.btn3
        val result = binding.result
        val question = binding.question
        result.setVisibility(View.INVISIBLE)
        val rannum1: Int = Random().nextInt(9)+1
        val rannum2: Int = Random().nextInt(9)+1
        num1.text=rannum1.toString()
        num2.text=rannum2.toString()
        question.setText("Choose one answer is correct?")

        val sum = rannum1 + rannum2
        val ranChoice: Int = (1 until 4).random()
        val sumMM = sum - 2
        val sumM = sum - 1
        val sumP = sum + 1
        val sumPP = sum + 2
        if (ranChoice == 1) {
            btn1.setText(sum.toString())
            btn2.setText(sumP.toString())
            btn3.setText(sumPP.toString())
        } else if (ranChoice == 2) {
            btn1.setText(sumM.toString())
            btn2.setText(sum.toString())
            btn3.setText(sumP.toString())
        } else if (ranChoice == 3) {
            btn1.setText(sumM.toString())
            btn2.setText(sumMM.toString())
            btn3.setText(sum.toString())
        }

        val true1 = btn1.getText()
        val true2 = btn2.getText()
        val true3 = btn3.getText()
        var trueBoolean: Boolean? = null

        fun checkTrue() {
            if (trueBoolean == true) {
                correct += 1
                trueScore.setText("Correct : " + correct.toString())
                Handler().postDelayed({
                    game(correct, incorrect)
                }, 1000)
            } else {
                incorrect += 1
                falseScore.setText("Incorrect : " + incorrect.toString())
            }
        }

        if ("$sum" == "$true1") {
            btn1.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn2.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn3.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        } else if ("$sum" == "$true2") {
            btn2.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn1.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn3.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        } else if ("$sum" == "$true3") {
            btn3.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn1.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn2.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        }

        val btnExit = binding.btnExit
        btnExit.setOnClickListener{
            val intent = Intent(PlusActivity@this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}