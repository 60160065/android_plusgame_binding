package buu.phatcharapol.calculator

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.Button
import android.widget.TextView

class MinusActivity : AppCompatActivity() {
    var correct: Int = 0
    var incorrect: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_minus)
        game(correct, incorrect)
    }
    fun game(correctScore: Int, incorrectScore: Int) {
        val trueScore = findViewById<TextView>(R.id.trueScore)
        trueScore.setText("Correct : " + correct.toString())
        val falseScore = findViewById<TextView>(R.id.falseScore)
        falseScore.setText("Incorrect : " + incorrect.toString())

        val num1 = findViewById<TextView>(R.id.num1)
        val num2 = findViewById<TextView>(R.id.num2)
        val btn1 = findViewById<Button>(R.id.btn1)
        val btn2 = findViewById<Button>(R.id.btn2)
        val btn3 = findViewById<Button>(R.id.btn3)
        val result = findViewById<TextView>(R.id.result)
        val question = findViewById<TextView>(R.id.question)
        result.setVisibility(View.INVISIBLE)
        val rannum1: Int = (0 until 10).random()
        val rannum2: Int = (0 until 10).random()
        num1.setText(rannum1.toString())
        num2.setText(rannum2.toString())
        question.setText("Choose one answer is correct?")

        val minus = rannum1 - rannum2
        val ranChoice: Int = (1 until 4).random()
        val minusMM = minus - 2
        val minusM = minus - 1
        val minusP = minus + 1
        val minusPP = minus + 2
        if (ranChoice == 1) {
            btn1.setText(minus.toString())
            btn2.setText(minusP.toString())
            btn3.setText(minusPP.toString())
        } else if (ranChoice == 2) {
            btn1.setText(minusM.toString())
            btn2.setText(minus.toString())
            btn3.setText(minusP.toString())
        } else if (ranChoice == 3) {
            btn1.setText(minusM.toString())
            btn2.setText(minusMM.toString())
            btn3.setText(minus.toString())
        }

        val true1 = btn1.getText()
        val true2 = btn2.getText()
        val true3 = btn3.getText()
        var trueBoolean: Boolean? = null

        fun checkTrue() {
            if (trueBoolean == true) {
                correct += 1
                trueScore.setText("Correct : " + correct.toString())
                Handler().postDelayed({
                    game(correct, incorrect)
                }, 1000)
            } else {
                incorrect += 1
                falseScore.setText("Incorrect : " + incorrect.toString())
            }
        }

        if ("$minus" == "$true1") {
            btn1.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn2.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn3.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        } else if ("$minus" == "$true2") {
            btn2.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn1.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn3.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        } else if ("$minus" == "$true3") {
            btn3.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn1.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn2.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        }
        val btnExit = findViewById<Button>(R.id.btnExit)
        btnExit.setOnClickListener{
            val intent = Intent(MinusActivity@this, MainActivity::class.java)
            startActivity(intent)
        }
    }
}