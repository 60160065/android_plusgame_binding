package buu.phatcharapol.calculator

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.phatcharapol.calculator.databinding.FragmentPlusBinding
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [PlusFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentPlusBinding
    private var correct = Score.Count().CountCorrect
    private var incorrect = Score.Count().CountIncorrect

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_plus,
            container,
            false
        )
        game()
        return binding.root
    }

    private fun game() {
        val trueScore = binding.trueScore
        trueScore.setText("Correct : " + correct.toString())
        val falseScore = binding.falseScore
        falseScore.setText("Incorrect : " + incorrect.toString())

        val num1 = binding.num1
        val num2 = binding.num2
        val btn1 = binding.btn1
        val btn2 = binding.btn2
        val btn3 = binding.btn3
        val result = binding.result
        val question = binding.question
        result.setVisibility(View.INVISIBLE)
        val rannum1: Int = Random().nextInt(9)+1
        val rannum2: Int = Random().nextInt(9)+1
        num1.text=rannum1.toString()
        num2.text=rannum2.toString()
        question.setText("Choose one answer is correct?")

        val sum = rannum1 + rannum2
        val ranChoice: Int = Random().nextInt(2)+1
        val sumMM = sum - 2
        val sumM = sum - 1
        val sumP = sum + 1
        val sumPP = sum + 2
        if (ranChoice == 1) {
            btn1.text = sum.toString()
            btn2.text = sumP.toString()
            btn3.text = sumPP.toString()
        } else if (ranChoice == 2) {
            btn1.text = sumM.toString()
            btn2.text = sum.toString()
            btn3.text = sumP.toString()
        } else if (ranChoice == 3) {
            btn1.text = sumM.toString()
            btn2.text = sumMM.toString()
            btn3.text = sum.toString()
        }

        val true1 = btn1.getText()
        val true2 = btn2.getText()
        val true3 = btn3.getText()
        var trueBoolean: Boolean? = null

        fun checkTrue() {
            if (trueBoolean == true) {
                correct += 1
                trueScore.setText("Correct : " + correct.toString())
                Handler().postDelayed({
                    game()
                }, 1000)
            } else {
                incorrect += 1
                falseScore.setText("Incorrect : " + incorrect.toString())
            }
        }

        if ("$sum" == "$true1") {
            btn1.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn2.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn3.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        } else if ("$sum" == "$true2") {
            btn2.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn1.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn3.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        } else if ("$sum" == "$true3") {
            btn3.setOnClickListener {
                result.setText("Correct")
                result.setVisibility(View.VISIBLE)
                trueBoolean = true
                checkTrue()
            }
            btn1.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
            btn2.setOnClickListener {
                result.setText("Incorrect")
                result.setVisibility(View.VISIBLE)
                trueBoolean = false
                checkTrue()
            }
        }
        binding.btnExit.setOnClickListener{
            it.findNavController().navigate(R.id.action_plusFragment_to_titleFragment)
        }
    }
}